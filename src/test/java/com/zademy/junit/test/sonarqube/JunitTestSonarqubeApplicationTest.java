package com.zademy.junit.test.sonarqube;


import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
class JunitTestSonarqubeApplicationTest {

	@Test
	void test() {
		JunitTestSonarqubeApplication.main(new String[] {});
	}

}
