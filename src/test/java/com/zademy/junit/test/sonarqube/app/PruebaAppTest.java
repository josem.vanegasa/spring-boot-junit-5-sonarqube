package com.zademy.junit.test.sonarqube.app;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class PruebaAppTest {
	
	private PruebaApp prueba = new PruebaApp();

	@Test
	void testPrueba() {
		
		int resultado = prueba.suma(10, 20);
		
		assertEquals(30, resultado);
		
	}

}
