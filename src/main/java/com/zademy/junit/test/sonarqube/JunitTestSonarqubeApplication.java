package com.zademy.junit.test.sonarqube;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class JunitTestSonarqubeApplication {

	public static void main(String[] args) {
		SpringApplication.run(JunitTestSonarqubeApplication.class, args);
	}

}
