## Pre-requisitos
- Spring Boot 2.6.6
- Spring 5.3.18
- Tomcat embed 9.0.60
- Maven 3.8.4
- Java 11

## Construir proyecto
```
mvn clean install
```

## Construir proyecto con SonarQube
```
mvn clean verify sonar:sonar -Dsonar.projectKey=com.zademy:junit.test.sonarqube -Dsonar.host.url=http://localhost:9000 -Dsonar.login=9616b964dc9a1ef7bf5feca55a85fdcbc5000af1
```

